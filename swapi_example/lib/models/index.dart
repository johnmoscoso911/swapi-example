import 'package:flutter/material.dart';
import 'package:swapi_example/ui/people/index.dart';
import 'package:swapi_example/ui/planets/index.dart';

export 'people.dart';

class OptionModel {
  final String title, asset, route;
  final Color color;

  const OptionModel(this.title, this.asset, this.color, this.route);
}

const List<OptionModel> menu = [
  OptionModel(
      'People', 'images/people.jpg', Color(0xFFc89b7b), PeopleScreen.ROUTE),
  OptionModel(
      'Planets', 'images/planets.jpg', Color(0xFF9bc4bc), PlanetsScreen.ROUTE),
  OptionModel(
      'Films', 'images/films.jpg', Color(0xFFd3ffe9), PeopleScreen.ROUTE),
  OptionModel(
      'Species', 'images/species.jpg', Color(0xFF8ddbe0), PeopleScreen.ROUTE),
  OptionModel(
      'Vehicles', 'images/vehicles.jpg', Color(0xFFce7da5), PeopleScreen.ROUTE),
  OptionModel('Starships', 'images/starships.jpg', Color(0xFFffd1ba),
      PeopleScreen.ROUTE),
];
