import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class PeopleModel extends Equatable {
  final Map<String, dynamic> payload;

  PeopleModel({this.payload});

  String get name => payload['name'];
  int get height => payload['height'];

  int get mass => payload['mass'];
  String get hairColor => payload['hair_color'];
  String get skinColor => payload['skin_color'];
  String get eyeColor => payload['eye_color'];
  String get birthYear => payload['birth_year'];
  String get gender => payload['gender'];
  String get homeworld => payload['homeworld'];
  List<dynamic> get films => payload['films'];
  // List<String>data=payload['films'];
  List<dynamic> get species => payload['species'];
  List<dynamic> get vehicles => payload['vehicles'];
  List<dynamic> get starships => payload['starships'];
  // String get =>payload['created": "2014-12-09T13:50:51.644000Z",
  // String get =>payload['edited": "2014-12-20T21:17:56.891000Z",
  String get url => payload['url'];

  @override
  List<Object> get props => payload.keys.toList();
}
