import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:swapi_example/common/index.dart';
import 'package:swapi_example/models/people.dart';
import 'package:swapi_example/ui/people/index.dart';

class PeopleScreen extends StatefulWidget {
  static const String ROUTE = 'people';
  @override
  _PeopleScreenState createState() => _PeopleScreenState();
}

class _PeopleScreenState extends State<PeopleScreen> {
  GetPeopleBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = GetPeopleBloc();
    _bloc.add(BaseEvent(url: Services.people));
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    String image = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('People'),
        // actions: <Widget>[
        //   IconButton(icon: Icon(FontAwesomeIcons.stepForward), onPressed: null)
        // ],
      ),
      body: BlocProvider<GetPeopleBloc>(
        create: (context) => _bloc,
        child: BlocListener<GetPeopleBloc, BaseState>(
          listener: (context, state) {
            if (state.hasError)
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(state.message),
              ));
          },
          child: BlocBuilder<GetPeopleBloc, BaseState>(
            builder: (context, state) {
              if (state.isLoading)
                return Center(child: CircularProgressIndicator());
              if (state.result != null) {
                if (state.result.count == 0)
                  return Center(child: Text('No existen datos para mostrar'));
                var data = state.result.results
                    .map((e) => PeopleModel(payload: e))
                    .toList();
                return Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Provider<String>(
                      create: (context) => image,
                      child: SingleChildScrollView(
                        child: Column(
                          children: data
                              .map((e) => SmallPeopleWidget(
                                    person: e,
                                  ))
                              .toList(),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 16.0,
                      right: 16.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16.0),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 8.0),
                          // decoration: BoxDecoration(
                          //   backgroundBlendMode: BlendMode.darken,
                          //   color: theme.primaryColor.withOpacity(0.45),
                          // ),
                          child: Row(
                            children: <Widget>[
                              state.result.previous != null
                                  ? GestureDetector(
                                      child: Icon(FontAwesomeIcons.stepBackward,
                                          color: theme.primaryColor),
                                      onTap: () => _bloc.add(
                                        BaseEvent(url: state.result.previous),
                                      ),
                                    )
                                  : SizedBox(),
                              SizedBox(
                                  width: state.result.next != null ? 8.0 : 0.0),
                              state.result.next != null
                                  ? GestureDetector(
                                      child: Icon(FontAwesomeIcons.stepForward,
                                          color: theme.primaryColor),
                                      onTap: () => _bloc.add(
                                        BaseEvent(url: state.result.next),
                                      ),
                                    )
                                  : SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }
              return Center();
            },
          ),
        ),
      ),
    );
  }
}
