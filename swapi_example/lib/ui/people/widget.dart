import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:swapi_example/models/index.dart';

class SmallPeopleWidget extends StatelessWidget {
  final PeopleModel person;

  const SmallPeopleWidget({Key key, @required this.person})
      : assert(person != null),
        super(key: key);

  list(List<dynamic> d) => ClipRRect(
        borderRadius: BorderRadius.circular(16.0),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(),
          child: Column(
            children: d?.map((e) => Text('$e'))?.toList(),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    var asset = Provider.of<String>(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Card(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: size.width * 0.2,
              height: size.width * 0.2,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Image.asset(asset, fit: BoxFit.cover),
              ),
            ),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                // mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: person.name,
                          style: textTheme.bodyText2
                              .copyWith(fontWeight: FontWeight.w700),
                          children: <TextSpan>[
                            TextSpan(
                              text: ' (${person.birthYear})',
                              style: textTheme.bodyText2,
                            )
                          ],
                        ),
                      ),
                      Icon(person.gender == 'female'
                          ? FontAwesomeIcons.venus
                          : FontAwesomeIcons.mars),
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        list(person.films),
                        list(person.species),
                        list(person.starships),
                        list(person.vehicles),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
