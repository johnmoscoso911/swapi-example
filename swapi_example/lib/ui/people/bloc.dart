import 'package:bloc/bloc.dart';
import 'package:swapi_example/common/index.dart';

class GetPeopleBloc extends Bloc<BaseEvent, BaseState> {
  @override
  BaseState get initialState => BaseState.init();

  @override
  Stream<BaseState> mapEventToState(
    BaseEvent event,
  ) async* {
    yield BaseState.loading();
    try {
      var result = await SwapiClient.get(event.url);
      yield BaseState.success(result);
    } catch (_) {
      yield BaseState.error(_.toString());
    }
  }
}
