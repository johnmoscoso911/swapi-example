import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swapi_example/models/index.dart';
import 'package:swapi_example/ui/people/index.dart';
import 'package:swapi_example/ui/planets/index.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: Colors.transparent,
    ),
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryTextTheme:
            GoogleFonts.snigletTextTheme(Theme.of(context).primaryTextTheme),
        textTheme: GoogleFonts.snigletTextTheme(Theme.of(context).textTheme),
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: {
        PeopleScreen.ROUTE: (context) => PeopleScreen(),
        PlanetsScreen.ROUTE: (context) => PlanetsScreen(),
        //TODO: agregar cada ruta
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentPage = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: currentPage,
      keepPage: false,
      viewportFraction: 0.8,
    );
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height * 0.6;
    var duration = Duration(milliseconds: 500);
    // var padding = MediaQuery.of(context).viewPadding;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          AnimatedContainer(
            duration: duration,
            color: menu[currentPage].color,
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                // padding: EdgeInsets.only(top: padding.top),
                height: height,
                child: PageView.builder(
                  itemBuilder: (context, index) => header(height, index),
                  controller: _pageController,
                  pageSnapping: true,
                  onPageChanged: (i) => setState(() => currentPage = i),
                  itemCount: menu.length,
                  physics: ClampingScrollPhysics(),
                ),
              ),
              AnimatedSwitcher(
                duration: duration,
                transitionBuilder: (child, animation) => ScaleTransition(
                  child: child,
                  scale: animation,
                ),
                child: GestureDetector(
                  // onTap: () => Navigator.of(context)
                  //     .push(MaterialPageRoute(builder: (_) => PeopleScreen())),
                  onTap: () => Navigator.of(context).pushNamed(
                      menu[currentPage].route,
                      arguments: menu[currentPage].asset),
                  child: Text(
                    menu[currentPage].title,
                    key: ValueKey<String>(menu[currentPage].title),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              SizedBox(height: height * 0.1),
            ],
          ),
        ],
      ),
    );
  }

  header(double height, int index) => AnimatedBuilder(
        animation: _pageController,
        builder: (context, child) {
          double value = 1;
          if (_pageController.position.haveDimensions) {
            value = _pageController.page - index;
            value = (1 - (value.abs() * 0.5)).clamp(0.0, 1.0);
            return Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 10.0),
                  height: Curves.easeIn.transform(value) * height,
                  child: child,
                ));
          } else {
            return Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 10.0),
                  height: Curves.easeIn
                          .transform(index == 0 ? value : value * 0.5) *
                      height,
                  child: child,
                ));
          }
        },
        child: Material(
          elevation: 4.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15.0),
                bottomRight: Radius.circular(15.0),
              ),
              child: Image.asset(
                menu[index].asset,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      );
}
