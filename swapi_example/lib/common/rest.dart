import 'package:http/http.dart' as http;
import 'dart:convert';

class RestModel {
  final Map<String, dynamic> payload;

  int get count => payload['count'];
  String get next => payload['next'];
  String get previous => payload['previous'];
  List<dynamic> get results => payload['results'];
  //   List<dynamic> data = payload['results'];
  //   return data?.map((e) => e)?.toList();
  // }

  // List<T>get data=>results?.map((e) => T())?.toList();

  RestModel({this.payload});
}

class Services {
  static const String url = 'https://swapi.co/api';
  static const String people = '$url/people/';
  static const String planets = '$url/planets/';
  static const String films = '$url/films/';
  static const String species = '$url/species/';
  static const String vehicles = '$url/vehicles/';
  static const String starships = '$url/starships/';
}

class SwapiClient {
  static Future<RestModel> get(String url) async {
    RestModel result;
    var response =
        await http.get('$url', headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      result = RestModel(payload: json.decode(utf8.decode(response.bodyBytes)));
    } else
      print(response.reasonPhrase);

    return result;
  }
}
