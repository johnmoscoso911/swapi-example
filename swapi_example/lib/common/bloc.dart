import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:swapi_example/common/index.dart';

@immutable
class BaseEvent extends Equatable {
  final String url;

  BaseEvent({this.url});

  @override
  List<Object> get props => null;
}

@immutable
class BaseState extends Equatable {
  final bool isLoading, hasError;
  final String message;
  final RestModel result;

  BaseState(
      {this.result,
      this.isLoading = false,
      this.hasError = false,
      this.message});

  factory BaseState.init() => BaseState();

  factory BaseState.loading() => BaseState(isLoading: true);

  factory BaseState.error(String message) =>
      BaseState(hasError: true, message: message);

  factory BaseState.success(RestModel result) => BaseState(result: result);

  @override
  List<Object> get props => [result, isLoading, hasError, message];
}
